<?php
include '../config.php';

$show_query = "SELECT * from shows";
$year = $_POST["year"];

$shows_result = mysqli_query($con, $show_query); //grab all shows from database
while($row = mysqli_fetch_assoc($shows_result))
{
     $startYear = intval(explode("-", $row["Start_Date"])[0]);
     if($year == $startYear)
     {
        echo "<tr>";
        echo "<td id='ShowName".$row['show_id']."' class='ShowName'> ".$row["ShowName"]."</td>";
        echo "<td id='showStartDate".$row['show_id']."' class='showStartDate'> ".$row["Start_Date"]."</td>";
        echo "<td id='showEndDate".$row['show_id']."' class='showEndDate'> ".$row["End_Date"]."</td>";
        echo "<td id='showEditButton".$row['show_id']."'> <button class='edit' id='" . $row['show_id'] . "' onclick='editShow(this.id)'>Edit</button></td>";
        echo "<td> <button class='delete' id='" . $row['show_id'] . "' onclick='deleteShow(this.id)'>Delete</button></td>";
        echo"</tr>";
     }
}

<?php

if (isset($_FILES["file"]["type"])) {
    $fullRename = $_POST["nameChange"]; //taking user input to change the name
    $validextensions = array("jpeg", "jpg", "png");
    $temporary = explode(".", $_FILES["file"]["name"]);
    $file_extension = end($temporary);
    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
        ) && ($_FILES["file"]["size"] < 10000000)//Approx. 10MB files can be uploaded.
        && in_array($file_extension, $validextensions)) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
            if (file_exists("../../images/" . $fullRename . ".png")) {
                echo $fullRename . ".png" . " already exists.";
            } else {

                $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                $targetPath = "../../images/" . $_FILES['file']['name']; // Target path where file is to be stored
                $finalName = $_FILES['file']['name']; //Checks to see if the file has been moved correctly so it can be renamed
                move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file

                rename("../../images/" . $finalName, "../../images/" . $fullRename . ".png");

                echo "Image uploaded successfully!";
            }
        }
    } else {
        echo "Invalid file size or type.";
    }
}

<?php

if (isset($_FILES["file2"]["type"])) {
    $fullRename = $_POST["nameChange"]; //taking user input to change the name
    $validextensions = array("jpeg", "jpg", "png");
    $temporary = explode(".", $_FILES["file2"]["name"]);
    $file_extension = end($temporary);
    if ((($_FILES["file2"]["type"] == "image/png") || ($_FILES["file2"]["type"] == "image/jpg") || ($_FILES["file2"]["type"] == "image/jpeg")
        ) && ($_FILES["file2"]["size"] < 1000000)//so that to large of images can't be uploaded.
        && in_array($file_extension, $validextensions)) {
        if ($_FILES["file2"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file2"]["error"] . "<br/><br/>";
        } else {
            if (file_exists("../../images/" . $fullRename.".png")) {
                echo $fullRename.".png"." already exists.";
            } else {

                $sourcePath = $_FILES['file2']['tmp_name']; // Storing source path of the file in a variable
                $targetPath = "../../images/" . $_FILES['file2']['name']; // Target path where file is to be stored
                $finalName = $_FILES['file2']['name']; //Checks to see if the file has been moved correctly so it can be renamed
                move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file

                rename("../../images/".$finalName,"../../images/". $fullRename.".png");

				echo "Image Uploaded Successfully!";
            }
        }
    } else {
        echo "Invalid file Size or Type.";
    }
}


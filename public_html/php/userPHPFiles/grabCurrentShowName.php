<?php
include "../config.php";

$day = date('Y-m-d',strtotime($_POST['day']));
$name = "";

$show_query = "select ShowName as name, End_Date as end, Start_Date as start from shows;";
$result = mysqli_query($con, $show_query);
$found = false;
while ($shows = mysqli_fetch_assoc($result)) { //while loop to find show id with closest end date to current day

    $showEnd = date('Y-m-d',strtotime($shows['end']));//grab ending date
    $showStart = date('Y-m-d',strtotime($shows['start']));//grab starting date

    if($showStart <= $day && $day <= $showEnd){
        $found = true;
        $name = $shows['name'];
        if($showStart == $showEnd){
            break;
        }
    }
}
if($found){
    echo $name;
}else{
    echo "No show found for selected date.";
}
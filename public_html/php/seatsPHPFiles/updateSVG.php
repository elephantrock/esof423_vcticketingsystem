<?php
include "../config.php";
include "./emptySeatArray.php";
$day = mysqli_real_escape_string($con, $_POST['day']);
$time = mysqli_real_escape_string($con, $_POST['time']);
$seat_query = "select seats as seat, Checked_in as checkin from reservations where Day='" . $day . "' and Time='".$time."'";
$grabbedSeats = mysqli_query($con, $seat_query);
//grab todays seats
while ($SeatsInreservation = mysqli_fetch_assoc($grabbedSeats)) {//one seat group per loop
    $seatSQL = $SeatsInreservation['seat'];//grab one seat group at a time, ex: B1-2
    $checkin =$SeatsInreservation['checkin'];
    if (strlen($seatSQL) > 6) {
        $seatGroupsArray = explode(",", $SeatsInreservation['seat']);//handle
    } else {//only one seat
        $seatGroupsArray = [$seatSQL];
    }
    foreach ($seatGroupsArray as $singleSeatGroup) {//grabs one row, then
        $singleSeatGroup = trim($singleSeatGroup);
        if (strlen($singleSeatGroup) < 4) {//one seat
            if(strlen($singleSeatGroup) == 2){//B1
                $seatNums = intval(substr($singleSeatGroup, 1, 1));
            }else{//B11
                $seatNums = intval(substr($singleSeatGroup, 1, 2));
            }
            if($checkin == 1){
                $seatNums = $seatNums."t";
            }
            else{
                $seatNums = $seatNums."f";
            }
            array_push($globalSeatArray[substr($singleSeatGroup, 0, 1)], $seatNums);
            //use the row letter as a key, push the seat numbers onto it
        } else {//multiple seats
            if(strlen($singleSeatGroup) > 5){//C10-12
                $startNum = intval(substr($singleSeatGroup, 1, 2));
                $endNum = intval(substr($singleSeatGroup, -2));
            }elseif(strlen($singleSeatGroup) == 5){//C9-12
                $startNum = intval(substr($singleSeatGroup, 1, 1));
                $endNum = intval(substr($singleSeatGroup, -2));
            }else{//C1-4
                $startNum = intval(substr($singleSeatGroup, 1, 1));
                $endNum = intval(substr($singleSeatGroup, -1));
            }

            $seatNumber="";
            for ($i = $startNum; $i < $endNum + 1; $i++) {
               // if checkIn==1, Checked_in is true
                if($checkin == "1"){
                   $seatNumber = $i."t";
                }
                // otherwise, Checked_in is false
                else{
                    $seatNumber = $i."f";
                }
                array_push($globalSeatArray[substr($singleSeatGroup, 0, 1)], $seatNumber);
            }
        }
    }
}

// $globalSeatArray would look like:
// [ "B" : ["1f", "2t", "3t", "4f"] ]

$keys = array_keys($globalSeatArray);
foreach ($keys as $key) {
    //for each row...
    if (count($globalSeatArray[$key]) > 0) {
        //if they have at least one seat...
        foreach ($globalSeatArray[$key] as $seats) {
            //create the ID and echo it
            // $seats will be that whole "1f" thing so we do a substring
            if(strlen($seats) == 2){
                $num = substr($seats, 0, 1);
            }else{
                $num = substr($seats, 0, 2);
            }
            $seatId = "seat_" . $num . "_" . $key . substr($seats, -1);
            echo $seatId . ", ";//give them the ID for the SVGs
        }
    }
}


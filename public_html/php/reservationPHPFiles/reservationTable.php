<?php
include '../config.php';

$date = mysqli_real_escape_string($con, $_POST['date']);
$title = $_POST['title'];
$time = $_POST['time'];
$sortColumn = $_POST['sort'];

$sql_query = "SELECT * FROM reservations JOIN customers c on c.customer_id = reservations.customer_id where Day='" . $date . "' and Time='".$time."'";
if(strlen($sortColumn) > 1){
    $sql_query = $sql_query." ORDER BY ".$sortColumn;
}
$result = mysqli_query($con, $sql_query);//grab all reservations from database
while ($row = mysqli_fetch_assoc($result)) {//display in each column

    echo "<tr id='reserve".$row['reservation_id']."'>";
    echo "<td><input class='checks' id='".$row['reservation_id']."' type='checkbox' name='reservationCheckbox' onchange='showPreview(".$row['reservation_id'].")'></td>";
    echo "<td id='lastName".$row['reservation_id']."'class='lastName'> ".$row["Last_Name"]."</td>";
    echo "<td id='firstName".$row['reservation_id']."' class='firstName'> ".$row["First_Name"]."</td>";
    echo "<td id='phone".$row['reservation_id']."' class='phone'> ".$row["phone"]."</td>";
    echo "<td id='time".$row['reservation_id']."' class='time'> ".$row["Time"]."</td>";
    echo "<td id='Adult_Tickets".$row['reservation_id']."' class='adultTickets'> ".$row["Adult_Tickets"]."</td>";
    echo "<td id='Discount_Tickets".$row['reservation_id']."' class='discountTickets'> ".$row["Discount_Tickets"]."</td>";
    echo "<td id='Child_Tickets".$row['reservation_id']."' class='childTickets'> ".$row["Child_Tickets"]."</td>";
    echo "<td id='Comp_Tickets".$row['reservation_id']."' class='compTickets'> ".$row["Comp_Tickets"]."</td>";
    echo "<td id='notes".$row['reservation_id']."' class='notes'> ".$row["Notes"]."</td>";
    echo "<td id= 'seatRequest".$row['reservation_id']."'class='seatRequest'> ".$row["Seat_Request"]."</td>";
    echo "<td id='seats".$row['reservation_id']."' class='seats'> ".$row["Seats"]."</td>";
    if (strcmp($title, "Reservations") === 0) {
        echo "<td id='reservationSource" . $row['reservation_id'] . "' class='reservationSource'> " . $row["Source"] . "</td>";
        if (strcmp($row["Checked_in"], "0") === 0) {
            echo "<td id='checkedIn" . $row['reservation_id'] . "' class='checkedIn'> No </td>";
        } else {
            echo "<td id='checkedIn" . $row['reservation_id'] . "' class='checkedIn'> Yes </td>";
        }
        echo "<td id='editDoneButton". $row['reservation_id'] ."'> <button class='edit' id='" . $row['reservation_id'] . "' onclick='editRes(this.id)'>Edit</button></td>";
        echo "<td> <button class='delete' id='" . $row['reservation_id'] . "' onclick='deleteRes(this.id)'>Delete</button></td>";
    }
    echo "</tr>";

}


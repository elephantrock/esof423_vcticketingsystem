$(document).ready(function (e) {
    $("#uploadimage").on('submit', (function (e) {
        e.preventDefault();
        $("#message").empty();
        $('#loading').show();
        $.ajax({
            url: "../php/manageSeasonPHPFiles/uploadSponsorImage.php", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData: false,        // To send DOMDocument or non processed data file it is set to false
            success: function (data)   // A function to be called if request succeeds
            {
                $('#loading').hide();
                alert(data);
            }
        });
    }));

    // Function to preview image after validation
    $(function () {
        $("#file").change(function () {
            $("#message").empty(); // To remove the previous error message
            var file = this.files[0];
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                $('#previewing').attr('src', 'noimage.png');
                $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                return false;
            } else {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });

    function imageIsLoaded(e) {
        $("#file").css("color", "green");
        $('#image_preview').css("display", "block");
        $('#previewing').attr('src', e.target.result);
        $('#previewing').attr('width', '250px');
        $('#previewing').attr('height', '230px');
    }
});


function displayShowAndSponsor()
{
    // Grabs value sent in for year by user+
    let year = document.getElementById("yearDropdown").value;

    // Retrieves show information from the database
    $.ajax({
        url: '../php/manageSeasonPHPFiles/showTable.php',
        type:'post',
        data:{year:year},
        success:function(response)
        {
            $("#show-search-results").html(response);
        }
    });

    // Retrieves sponsor information from the database
    $.ajax({
        url: '../php/manageSeasonPHPFiles/sponsorTable.php',
        type:'post',
        data:{year:year},
        success:function(response)
        {
            $("#sponsor-search-results").html(response);
        }
    });
}

// Deletes a show with confirmation
function deleteShow(clicked_id){

    let showDeleteConfirmation = confirm("Are you sure you want to delete this show?");

    // If user answers yes, the show will be deleted from the database
    // Otherwise, the window will be closed and no further action will be taken
    if (showDeleteConfirmation === true)
    {
        $.ajax({
            url:'../php/manageSeasonPHPFiles/deleteShow.php',
            type:'post',
            data:{id:clicked_id},
            success:function(response)
            {
                $("#show-search-results").html(response);
                alert("Show deleted successfully!");
            },
            complete:function () {
                displayShowAndSponsor();
            }
        });
    }
    else
    {
        window.close();
    }

    // auto refreshes the table
    let button = document.getElementById("goButton");
    button.click();
}

// Edits show's information in the table displayed
function editShow(clicked_id)
{
    // Make the field for show name, start date, end date editable
    document.getElementById("ShowName".concat(clicked_id)).contentEditable=true;
    document.getElementById("showStartDate".concat(clicked_id)).contentEditable=true; 
    document.getElementById("showEndDate".concat(clicked_id)).contentEditable=true; 

    // Change the background color to show users that these fields are editable
    document.getElementById("ShowName".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("showStartDate".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("showEndDate".concat(clicked_id)).style.backgroundColor="white";

    // Change 'Edit' button to 'Done'
    document.getElementById("showEditButton".concat(clicked_id)).innerHTML = `<button class='done' id='${clicked_id}' onclick='DoneShow(this.id)'>Done</button>`
}

// Takes the edited show information and updates the database
function DoneShow(clicked_id)
{
    let newShowName = $('#ShowName'.concat(clicked_id)).html().trim()
    let newShowStartDate = document.getElementById("showStartDate".concat(clicked_id)).innerText;
    let newShowEndDate = document.getElementById("showEndDate".concat(clicked_id)).innerText;

    // sends new show info to the database
    $.ajax({
        url:'../php/manageSeasonPHPFiles/editShow.php',
        type:'post',
        data:{id:clicked_id,
            showName:newShowName,
            showStartDate: newShowStartDate,
            showEndDate:newShowEndDate},
        success:function(response){
            alert("Show edited successfully!");
        },
        complete:function () {
            displayShowAndSponsor();
        }
    });

    // Make the field for show name, start date, end date uneditable
    document.getElementById("ShowName".concat(clicked_id)).contentEditable=false;
    document.getElementById("showStartDate".concat(clicked_id)).contentEditable=false; 
    document.getElementById("showEndDate".concat(clicked_id)).contentEditable=false; 

    // Return background color to original 
    document.getElementById("ShowName".concat(clicked_id)).style.backgroundColor=null;
    document.getElementById("showStartDate".concat(clicked_id)).style.backgroundColor=null;
    document.getElementById("showEndDate".concat(clicked_id)).style.backgroundColor=null;

    // Change 'Done' button back to 'Edit'
    document.getElementById("showEditButton".concat(clicked_id)).innerHTML = `<button class='edit' id='${clicked_id}' onclick='editShow(this.id)'>Edit</button>`

    // auto refreshes the table
    let button = document.getElementById("goButton");
    button.click();
}


// Deletes a sponsor with confirmation
function deleteSponsor(clicked_id){

    let sponsorDeleteConfirmation = confirm("Are you sure you want to delete this sponsor?");

    // If user answers yes, the sponsor will be deleted from the database
    // Otherwise, the window will be closed and no further action will be taken
    if (sponsorDeleteConfirmation === true)
    {
        $.ajax({
            url:'../php/manageSeasonPHPFiles/deleteSponsor.php',
            type:'post',
            data:{id:clicked_id},
            success:function(response)
            {
                $("#sponsor-search-results").html(response);
                alert("Sponsor deleted successfully!");
            },
            complete:function () {
                displayShowAndSponsor();
            }
        });
    }
    else
    {
        window.close();
    }
    
    // auto refreshes the table
    let button = document.getElementById("goButton");
    button.click();
}

// Edits sponsor's information in the table displayed
function editSponsor(clicked_id)
{
    // Make the field for sponsor name, start date, end date editable
    document.getElementById("sponsorName".concat(clicked_id)).contentEditable=true;
    document.getElementById("sponsorStartDate".concat(clicked_id)).contentEditable=true; 
    document.getElementById("sponsorEndDate".concat(clicked_id)).contentEditable=true; 

    // Change the background color to show users that these fields are editable
    document.getElementById("sponsorName".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("sponsorStartDate".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("sponsorEndDate".concat(clicked_id)).style.backgroundColor="white";

    // Change 'Edit' button to 'Done'
    document.getElementById("sponsorEditButton".concat(clicked_id)).innerHTML = `<button class='done' id='${clicked_id}' onclick='DoneSponsor(this.id)'>Done</button>`
}

// Takes the edited sponsor information and updates the database
function DoneSponsor(clicked_id)
{
    let newSponsorName = $('#sponsorName'.concat(clicked_id)).html().trim()
    let newSponsorStartDate = document.getElementById("sponsorStartDate".concat(clicked_id)).innerText;
    let newSponsorEndDate = document.getElementById("sponsorEndDate".concat(clicked_id)).innerText;

    // sends new sponsor info to the database
    $.ajax({
        url:'../php/manageSeasonPHPFiles/editSponsor.php',
        type:'post',
        data:{id:clicked_id,
            sponsorName:newSponsorName,
            sponsorStartDate: newSponsorStartDate,
            sponsorEndDate:newSponsorEndDate},
        success:function(response){
            alert("Sponsor edited successfully!");
        },
        complete:function () {
            displayShowAndSponsor();
        }
    });

    // Make the field for show name, start date, end date uneditable
    document.getElementById("sponsorName".concat(clicked_id)).contentEditable=false;
    document.getElementById("sponsorStartDate".concat(clicked_id)).contentEditable=false; 
    document.getElementById("sponsorEndDate".concat(clicked_id)).contentEditable=false; 

    // Return background color to original 
    document.getElementById("sponsorName".concat(clicked_id)).style.backgroundColor=null;
    document.getElementById("sponsorStartDate".concat(clicked_id)).style.backgroundColor=null;
    document.getElementById("sponsorEndDate".concat(clicked_id)).style.backgroundColor=null;

    // Change 'Done' button back to 'Edit'
    document.getElementById("sponsorEditButton".concat(clicked_id)).innerHTML = `<button class='edit' id='${clicked_id}' onclick='editSponsor(this.id)'>Edit</button>`

    // auto refreshes the table
    let button = document.getElementById("goButton");
    button.click();
}
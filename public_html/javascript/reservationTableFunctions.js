function deleteRes(clicked_id){//deletes reservation
   
   let reservationDeleteConfirmation = confirm("Are you sure you want to delete this reservation? This can't be undone.");
   if (reservationDeleteConfirmation === true)
   {
        $.ajax({
            url:'../php/reservationPHPFiles/deleteReservation.php',
            type:'post',
            data:{id:clicked_id},
            success:function(response){
                alert(response);
            },
            complete:function () {
                searchFunction("Reservations");
            }
        });
   }
   else
   {
        window.close();
   }
    
    let button = document.getElementById("goButton");
    button.click();
}

function editRes(clicked_id){//calls for a table to show up for editing purposes

    document.getElementById("firstName".concat(clicked_id)).contentEditable=true;
    document.getElementById("lastName".concat(clicked_id)).contentEditable=true;
    document.getElementById("phone".concat(clicked_id)).contentEditable=true;
    document.getElementById("time".concat(clicked_id)).contentEditable=true;
    document.getElementById("Adult_Tickets".concat(clicked_id)).contentEditable=true;
    document.getElementById("Discount_Tickets".concat(clicked_id)).contentEditable=true;
    document.getElementById("Child_Tickets".concat(clicked_id)).contentEditable=true;
    document.getElementById("Comp_Tickets".concat(clicked_id)).contentEditable=true;
    document.getElementById("notes".concat(clicked_id)).contentEditable=true;
    document.getElementById("seatRequest".concat(clicked_id)).contentEditable=true;
    document.getElementById("seats".concat(clicked_id)).contentEditable=true;

    document.getElementById("firstName".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("lastName".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("phone".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("time".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("Adult_Tickets".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("Discount_Tickets".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("Child_Tickets".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("Comp_Tickets".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("notes".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("seatRequest".concat(clicked_id)).style.backgroundColor="white";
    document.getElementById("seats".concat(clicked_id)).style.backgroundColor="white";



    document.getElementById("editDoneButton".concat(clicked_id)).innerHTML = `<button class='done' id='${clicked_id}' onclick='DoneRes(this.id)'>Done</button>`


}
function DoneRes(clicked_id){//finishes editing reservation and writes it to the database
    let firstName = $('#firstName'.concat(clicked_id)).html().trim()
    let lastName = $('#lastName'.concat(clicked_id)).html().trim()
    let phone = $('#phone'.concat(clicked_id)).html().trim()
    let time = $('#time'.concat(clicked_id)).html().trim()
    let adultTickets = $('#Adult_Tickets'.concat(clicked_id)).html().trim()
    let discountTickets = $('#Discount_Tickets'.concat(clicked_id)).html().trim()
    let childTickets = $('#Child_Tickets'.concat(clicked_id)).html().trim()
    let compTickets = $('#Comp_Tickets'.concat(clicked_id)).html().trim()
    let notes = $('#notes'.concat(clicked_id)).html().trim()
    let seatRequest = $('#seatRequest'.concat(clicked_id)).html().trim()
    let seats = $('#seats'.concat(clicked_id)).html().trim()

    $.ajax({//ajax request to modify the reservation info in the database
        url:'../php/reservationPHPFiles/editReservation.php',
        type:'post',
        data:{id:clicked_id,firstName:firstName,lastName:lastName,
            phone:phone,time:time,adultTickets:adultTickets,
            discountTickets:discountTickets, childTickets:childTickets,
            compTickets:compTickets,notes:notes,seatRequest:seatRequest, seats:seats},
        success:function(response){
            alert(response);
        },
        complete:function () {

            document.getElementById("firstName".concat(clicked_id)).contentEditable=false;
            document.getElementById("lastName".concat(clicked_id)).contentEditable=false;
            document.getElementById("phone".concat(clicked_id)).contentEditable=false;
            document.getElementById("time".concat(clicked_id)).contentEditable=false;
            document.getElementById("Adult_Tickets".concat(clicked_id)).contentEditable=false;
            document.getElementById("Discount_Tickets".concat(clicked_id)).contentEditable=false;
            document.getElementById("Child_Tickets".concat(clicked_id)).contentEditable=false;
            document.getElementById("Comp_Tickets".concat(clicked_id)).contentEditable=false;
            document.getElementById("notes".concat(clicked_id)).contentEditable=false;
            document.getElementById("seatRequest".concat(clicked_id)).contentEditable=false;
            document.getElementById("seats".concat(clicked_id)).contentEditable=false;

            document.getElementById("firstName".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("lastName".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("phone".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("time".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("Adult_Tickets".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("Discount_Tickets".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("Child_Tickets".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("Comp_Tickets".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("notes".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("seatRequest".concat(clicked_id)).style.backgroundColor=null;
            document.getElementById("seats".concat(clicked_id)).style.backgroundColor=null;

            searchFunction("Reservations");
        }
    });

    document.getElementById("editDoneButton".concat(clicked_id)).innerHTML = `<button class='edit' id='${clicked_id}' onclick='editRes(this.id)'>Edit</button>`

    let button = document.getElementById("goButton");
    button.click();

}

// Checks in reservations that have been picked up
function checkInReservation(){
    checkboxes = document.getElementsByName('reservationCheckbox');
    // need to grab reservation id for every checkbox that's checked
    for (var i = 0; i < checkboxes.length; i++) {
        if(checkboxes[i].checked){
            $.ajax({
                url: '../php/reservationPHPFiles/checkInReservation.php',
                type:'post',
                data:{id:checkboxes[i].id},
                
                complete:function(){
                    searchFunction("Reservations");
                }
            });
            
            // auto-refresh seating chart
            let button = document.getElementById("goButton");
            button.click();
        }
    } 
}

function unCheckInReservation() {
    checkboxes = document.getElementsByName('reservationCheckbox');
    
    // grab reservation id for every checkbox that's checked
    // and send it in to uncheck-in reservation
    for (var i = 0; i < checkboxes.length; i++) {
        if(checkboxes[i].checked){
            $.ajax({
                url: '../php/reservationPHPFiles/unCheckInReservation.php',
                type:'post',
                data:{id:checkboxes[i].id},
                
                complete:function(){
                    searchFunction("Reservations");
                }
            });
            
            // auto-refresh seating chart
            let button = document.getElementById("goButton");
            button.click();
        }
    }
}

function sortReservations(checkedBoxId, sourcePage){
    if(checkedBoxId === "LastNameSortBox"){
        document.getElementById("SeatsSortBox").checked = false;
    }else{
        document.getElementById("LastNameSortBox").checked = false;
    }
    searchFunction(sourcePage);
}

function searchFunction(title){//populates reservation table with reservations
    let date = document.getElementById("datepicker").value;
    let time = document.getElementById("timeSelect").value;
    let sortColumn = "";

    if(document.getElementById("SeatsSortBox").checked === true){
        sortColumn = "Seats"
    }else if(document.getElementById("LastNameSortBox").checked === true){
        sortColumn = "Last_Name"
    }

    if(title === "Generate Tickets"){
        document.getElementById("tickPrev").innerHTML = "";
        document.getElementById("backTicketsPrev").innerHTML = "";
    }
    $.ajax({
        url:'../php/reservationPHPFiles/reservationTable.php',
        type:'post',
        data:{date:date,
              title:title,
              time:time,
              sort:sortColumn},
        success:function(response){
            $("#search-results").html(response);
        }
    });
    if(title === "Reservations"){//if page is reservations, updates seating chart
        let seatIds = "test";
        cleanSVG(".seatingPreview");
        $.ajax({//ajax request that grabs an array of IDs to change colors
            url:'../php/seatsPHPFiles/updateSVG.php',
            type:'post',
            data:{day:date,
                  time:time},
            success:function(response){
                seatIds = response;
                seatIds = seatIds.slice(0,seatIds.length-2); //takes of a ", " at the end
                if(seatIds.length > 10){//multiple seats
                    seatIds = seatIds.split(", ");
                    seatIds.forEach(function(value){
                        // take the t/f off of the id
                        let marker = value.charAt(value.length-1);
                        value = value.slice(0, value.length-1); //take off t/f
                        if (marker === "t"){
                            document.getElementById(value).style.fill = "green";
                        } else {
                            document.getElementById(value).style.fill = "yellow";
                        }
                    });
                }else{//one seat
                    let marker = seatIds.charAt(seatIds.length-1);
                    value = seatIds.slice(0, seatIds.length-1); //take off t/f
                    if (marker === "t"){
                        document.getElementById(seatIds).style.fill = "green";
                    } else {
                        document.getElementById(seatIds).style.fill = "yellow";
                    }
                }
            }
        });
    }
}

function showPreview(clicked_id) {
    let box = document.getElementById(clicked_id);
    if(box.checked){
        let name = $('#firstName'.concat(clicked_id)).html()
        name += " ".concat($('#lastName'.concat(clicked_id)).html())
        let date = document.getElementById("datepicker").value;
        let resId = "res".concat(clicked_id);
        let adultTickets = $('#Adult_Tickets'.concat(clicked_id)).html();
        let discountTickets = $('#Discount_Tickets'.concat(clicked_id)).html();
        let childTickets = $('#Child_Tickets'.concat(clicked_id)).html();
        let compTickets = $('#Comp_Tickets'.concat(clicked_id)).html();
        let totalTickets = parseInt(adultTickets) + parseInt(discountTickets) + parseInt(childTickets) + parseInt(compTickets);
        let seats = $('#seats'.concat(clicked_id)).html();
        //grabs variables for ajax request
        $.ajax({
            url: '../php/genTicketsPHPFiles/ticketGenTemplate.php',
            type: 'post',
            data: {
                name:name,
                adultTickets: adultTickets,
                discountTickets: discountTickets,
                childTickets: childTickets,
                compTickets: compTickets,
                seats: seats,
                res_id:resId,
                date:date
            },
            success: function (response) {
                $("#tickPrev").append(response);
                $.ajax({
                    url:"../php/genTicketsPHPFiles/genBackTickets.php",
                    type: 'post',
                    data:{date:date,
                        totalTickets:totalTickets,
                        resId:resId},
                    success:function (response){
                        $("#backTicketsPrev").append(response);
                    }
                });
            }
        });

    }else{//removes SVG if box is unchecked
        let SVGs = document.getElementsByClassName("res".concat(clicked_id));
        for(let k = SVGs.length - 1; k >= 0; k--){
            SVGs[k].remove();
        }
    }
    }

//function that changes which side of the ticket shows up
function flipTicketPreview(element){
    if(element.innerHTML === "Show Back"){
        element.innerHTML = "Show Front"
        document.getElementById("tickPrev").style.display = "none";
        document.getElementById("backTicketsPrev").style.display = null;

    }else{
        element.innerHTML = "Show Back"
        document.getElementById("tickPrev").style.display = null;
        document.getElementById("backTicketsPrev").style.display = "none";
    }
}

function printPreview(){
    let button = document.getElementById("flip")

    if(button.innerHTML === "Show Back"){
        document.getElementById("backTicketsPrev").style.display = null;
    }else{
        document.getElementById("tickPrev").style.display = null;
    }
    document.getElementById("bothPreviews").style.display = null;

    let frontTickets = document.getElementById("tickPrev").children;
    let backTickets = document.getElementById("backTicketsPrev").children;

    let numTicketsLeft = frontTickets.length;

    let ticketPointer = 0;
    let emptySVG = `<svg width='816' height='192'>
                    </svg>`;
    let spaceSVG = `<svg width='816' height='12'> </svg>`;

    let printDiv = document.getElementById("bothPreviews");
    printDiv.style.width = "100%";
    printDiv.style.height = "100%";
    printDiv.style.display = "table";
    printDiv.style.textAlign = "center";

    for(let i = 0; i < frontTickets.length; i += 6){
        if(numTicketsLeft < 5){
            for(let j = 0; j < numTicketsLeft; j++){
                printDiv.innerHTML += frontTickets[ticketPointer++].outerHTML;
                printDiv.innerHTML += spaceSVG;
            }
            for(let h = 0; h < 5 - numTicketsLeft; h++){
                printDiv.innerHTML += emptySVG;
                printDiv.innerHTML += spaceSVG;
            }
            ticketPointer = 0;
            for(let g = 0; g < numTicketsLeft; g++){
                printDiv.innerHTML += backTickets[ticketPointer++].outerHTML;
                printDiv.innerHTML += spaceSVG;
            }
        }else{
            printDiv.innerHTML += frontTickets[i].outerHTML;
            printDiv.innerHTML += spaceSVG;
            printDiv.innerHTML += frontTickets[i+1].outerHTML;
            printDiv.innerHTML += spaceSVG;
            printDiv.innerHTML += frontTickets[i+2].outerHTML;
            printDiv.innerHTML += spaceSVG;
            printDiv.innerHTML += frontTickets[i+3].outerHTML;
            printDiv.innerHTML += spaceSVG;
            printDiv.innerHTML += frontTickets[i+4].outerHTML;

            printDiv.innerHTML += backTickets[i].outerHTML;
            printDiv.innerHTML += spaceSVG;
            printDiv.innerHTML += backTickets[i+1].outerHTML;
            printDiv.innerHTML += spaceSVG;
            printDiv.innerHTML += backTickets[i+2].outerHTML;
            printDiv.innerHTML += spaceSVG;
            printDiv.innerHTML += backTickets[i+3].outerHTML;
            printDiv.innerHTML += spaceSVG;
            printDiv.innerHTML += backTickets[i+4].outerHTML;

            numTicketsLeft -= 5;
            ticketPointer += 5;
        }
    }

    printJS({
        printable:'bothPreviews',
        type: 'html',
        style: "div {width: 100%; height: 100%; display: table; padding:0; text-align: center;} svg{margin:0; padding:0;} @page{margin:0cm; padding:0cm; border:none;} "
    });
    if(button.innerHTML === "Show Back"){
        document.getElementById("backTicketsPrev").style.display = "none";
    }else{
        document.getElementById("tickPrev").style.display = "none";
    }
    document.getElementById("bothPreviews").innerHTML = "";
    document.getElementById("bothPreviews").style.display = "none";
}